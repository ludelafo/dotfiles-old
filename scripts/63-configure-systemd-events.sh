#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Create the directory
mkdir --parent "/etc/systemd/logind.conf.d"

# Actions when laptop lid is opened/closed
cat > "/etc/systemd/logind.conf.d/logind.conf" <<- EOL
	[Login]
	HandlePowerKey=poweroff
	HandleSuspendKey=suspend
	HandleHibernateKey=hibernate
	HandleLidSwitch=lock
	HandleLidSwitchDocked=ignore
	HandleLidSwitchExternalPower=lock
EOL

# Restart the services
systemctl restart systemd-logind.service