#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

pacman --sync --needed sudo

echo "%wheel ALL=(ALL) ALL" > "/etc/sudoers.d/wheel"

chmod -c 0440 "/etc/sudoers.d/wheel"