#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Set the console keyboard layout if preset
if [[ -n "${KEYMAP}" ]]; then
	echo "KEYMAP=${KEYMAP}" >> "/etc/vconsole.conf"
fi

echo "FONT=${FONT}" >> "/etc/vconsole.conf"
echo "FONT_MAP=${FONT_MAP}" >> "/etc/vconsole.conf"
