#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

cat > "/etc/systemd/network/20-${ETHERNET_INTERFACE_NEW_NAME}.network" <<- EOL
	[Match]
	Name=${ETHERNET_INTERFACE_NEW_NAME}

	[Network]
	DHCP=ipv4
	LinkLocalAddressing=ipv4
	IPv6AcceptRA=no
EOL

systemctl restart systemd-networkd
