#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

interfaceName=$(ip link show | grep --no-filename --only-matching "${WIRELESS_INTERFACE_REGEX}:" | rev | cut -c 2- | rev)

if [[ ${interfaceName} ]]; then
		interfaceMacAddress=$(cat "/sys/class/net/${interfaceName}/address")

		cat > "/etc/systemd/network/10-wireless0.link" <<- EOL
			[Match]
			MACAddress=${interfaceMacAddress}

			[Link]
			Description=Internal wireless interface
			Name=${WIRELESS_INTERFACE_NEW_NAME}
		EOL

		systemctl restart systemd-networkd
fi
