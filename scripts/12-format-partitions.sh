#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Format the partitions
mkfs.fat -n "${EFI_PARTITION_LABEL}" -F 32 "${EFI_BLOCK_DEVICE}"
mkfs.ext4 -F -L "${ROOT_PARTITION_LABEL}" "${ROOT_BLOCK_DEVICE}"
mkswap -L "${SWAP_PARTITION_LABEL}" "${SWAP_BLOCK_DEVICE}"
swapon "${SWAP_BLOCK_DEVICE}"