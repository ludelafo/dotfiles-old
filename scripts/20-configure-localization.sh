#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

for locale in ${LOCALES}; do
	sed --in-place "s/^#${locale}/${locale}/" "/etc/locale.gen"
done

locale-gen

cat > "/etc/locale.conf" <<- EOL
	LANG=${DEFINED_LANG}
	LC_TIME=${DEFINED_LC_TIME}
	LC_COLLATE=${DEFINED_LC_COLLATE}
EOL