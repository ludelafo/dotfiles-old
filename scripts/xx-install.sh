#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Load the constants
source "./00-config.sh"

# Load Swiss French keyboard layout
# loadkeys fr_CH-latin1

# Update the system clock
timedatectl set-ntp true

# Partition the disk
(
    echo "g" # Create a new empty GPT partition table

    # First partition (EFI)
    echo "n" # Add a new partition
    echo   # Partition number (accept default)
    echo   # First sector (accept default)
    echo "+${EFI_PARTITION_SIZE}" # Last sector

    # Second parition (root)
    echo "n" # Add a new partition
    echo   # Partition number (accept default)
    echo   # First sector (Accept default)
    echo "-${SWAP_PARTITION_SIZE}" # Last sector (all the available space minus the swap space)

    # Last partition (swap)
    echo "n" # Add a new partition
    echo   # Partition number (accept default)
    echo   # First sector (Accept default)
    echo   # Last sector (Accept default)

    # Change partition type for the first partition
    echo "t" # Change the partition type
    echo "1" # Partition number
    echo "1" # Partition type: EFÎ System

    # Change partition type for the second partition
    echo "t" # Change the partition type
    echo "2" # Partition number
    echo "20" # Partition type: Linux filesystem

    # Change partition type for the last partition
    echo "t" # Change the partition type
    echo "3" # Partition number
    echo "19" # Partition type: Linux swap

    echo "w" # Write changes
) | fdisk "${BASE_BLOCK_DEVICE}"

# Format the partitions
mkfs.fat -n "${EFI_PARTITION_LABEL}" -F 32 "${EFI_BLOCK_DEVICE}"
mkfs.ext4 -F -L "${ROOT_PARTITION_LABEL}" "${ROOT_BLOCK_DEVICE}"
mkswap -L "${SWAP_PARTITION_LABEL}" "${SWAP_BLOCK_DEVICE}"
swapon "${SWAP_BLOCK_DEVICE}"

# Mount the file systems
mount "${ROOT_BLOCK_DEVICE}" "/mnt"
mkdir "/mnt/boot"
mount "${EFI_BLOCK_DEVICE}" "/mnt/boot"

# Install packages
pacstrap "/mnt" ${CORE_PACKAGES}

# Generate the fstab
genfstab -L "/mnt" > "/mnt/etc/fstab"

# Copy the post-install scripts to root
cp "consts.sh" "/mnt"
cp "post-install.sh" "/mnt"

# Change root into the new system
arch-chroot "/mnt" "./post-install.sh"

# Delete the post-install scripts from root
rm "/mnt/consts.sh"
rm "/mnt/post-install.sh"

# Unmount all mounted partitions
umount --recursive "/mnt"

# Reboot
echo "Installation done. Will reboot (hopefully to your new system) in five seconds..."
sleep 5
reboot
