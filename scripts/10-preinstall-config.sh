#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

if [[ -n "${KEYMAP}" ]]; then
	# Load custom keyboard layout
	loadkeys "${KEYMAP}"
fi

# Update the system clock
timedatectl set-ntp true

# Connect to WiFi
if [[ "${CONNECT_TO_WIFI}" = true ]]; then
	cat > "/etc/wpa_supplicant/wpa_supplicant-${WIRELESS_INTERFACE_NEW_NAME}.conf" <<- EOL
		network={
			ssid="${WIFI_SSID}"
			psk=${WIFI_PSK}
		}
	EOL
fi
