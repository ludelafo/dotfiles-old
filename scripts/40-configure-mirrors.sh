#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Sort the mirrors list
reflector \
	--country Switzerland \
	--country Italy \
	--country France \
	--country Germany \
	--age 24 \
	--protocol https \
	--sort rate \
	--save /etc/pacman.d/mirrorlist

# Create the reflector hook
mkdir --parent "/etc/pacman.d/hooks"

cat > "/etc/pacman.d/hooks/mirrors-upgrade.hook" <<- EOL
	[Trigger]
	Operation = Upgrade
	Type = Package
	Target = pacman-mirrorlist

	[Action]
	Description = Updating pacman-mirrorlist with reflector and removing pacnew...
	When = PostTransaction
	Depends = reflector
	Exec = /bin/sh -c "reflector --country Switzerland --country Italy --country France --country Germany --age 24 --protocol https --sort rate --save /etc/pacman.d/mirrorlist"
EOL