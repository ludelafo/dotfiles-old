#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Copy the post-install scripts to root
cp *-*.sh "${MOUNT_POINT}"

# Copy wpa_supplicant file if presents
wpa_supplicant_file="/etc/wpa_supplicant/wpa_supplicant-${WIRELESS_INTERFACE_NEW_NAME}.conf"

if [ -f "${wpa_supplicant_file}" ]; then
	mkdir --parents "${MOUNT_POINT}/etc/wpa_supplicant"

	cp "${wpa_supplicant_file}" "${MOUNT_POINT}${wpa_supplicant_file}"
fi