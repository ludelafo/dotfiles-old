#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Configure pacman
sed --in-place "/\[multilib\]/,/Include/""s/^#//" "/etc/pacman.conf"
sed --in-place "s/#Color/Color/" "/etc/pacman.conf"
sed --in-place "s/#TotalDownload/TotalDownload/" "/etc/pacman.conf"

# Synchronize the mirrors
pacman --sync --refresh