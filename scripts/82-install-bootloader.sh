#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Install the boot loader
efibootmgr --disk ${BASE_BLOCK_DEVICE} --part ${EFI_PARTITION_NUMBER} --create --label "${EFI_BOOT_ENTRY_LABEL}" --loader "/vmlinuz-${LINUX_KERNEL}" --unicode "root=${ROOT_BLOCK_DEVICE} initrd=\initramfs-${LINUX_KERNEL}.img ${KERNEL_PARAMETERS}"

if [[ "$USE_STARTUP_NSH_SCRIPT" = true ]]; then
	echo "vmlinuz-${LINUX_KERNEL} root=${ROOT_BLOCK_DEVICE} initrd=\initramfs-${LINUX_KERNEL}.img ${KERNEL_PARAMETERS}" > "/boot/startup.nsh"
fi
