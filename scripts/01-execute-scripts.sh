#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

execute_scripts_from=${1}
execute_scripts_to=${2}

for (( \
	script_number = ${execute_scripts_from}; \
	script_number <= ${execute_scripts_to}; \
	script_number++ \
)); do
	script_file="${script_number}-*.sh"

	# Skip if script file not found
	if [ ! -f ${script_file} ]; then
		continue
	fi

	echo "Executing '${script_file}'..."
	${directory_path}/${script_file}
done
