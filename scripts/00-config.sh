#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Exit immediately if a command exits with a non-zero status
set -o errexit

# Treat unset variables as an error when substituting
set -o nounset

# The return value of a pipeline is the status of
# the last command to exit with a non-zero status,
# or zero if no command exited with a non-zero status
set -o pipefail

# TODO: terminal emulator, spell checking power management user directories (xresources), alias
# swap

BASE_BLOCK_DEVICE='/dev/nvme0n1'

# EFI PARTITION
EFI_PARTITION_NUMBER=1
EFI_PARTITION_SIZE="100M"
EFI_PARTITION_LABEL="EFI"
EFI_BOOT_ENTRY_LABEL="Arch Linux"

# Root PARTITION
ROOT_PARTITION_NUMBER=2
ROOT_PARTITION_LABEL="root"

# Swap partition
SWAP_PARTITION_NUMBER=3
SWAP_PARTITION_SIZE="2G"
SWAP_PARTITION_LABEL="swap"

# Create specific block device
EFI_BLOCK_DEVICE="${BASE_BLOCK_DEVICE}p${EFI_PARTITION_NUMBER}"
ROOT_BLOCK_DEVICE="${BASE_BLOCK_DEVICE}p${ROOT_PARTITION_NUMBER}"
SWAP_BLOCK_DEVICE="${BASE_BLOCK_DEVICE}p${SWAP_PARTITION_NUMBER}"

# Mount point
MOUNT_POINT="/mnt"

# WiFi
CONNECT_TO_WIFI=true
WIFI_SSID="TO_CHANGE"
WIFI_PSK="TO_CHANGE"

# Hostname
HOSTNAME="Arch-Linux-Lenovo"

# Users and groups
USER_USERNAME="ludelafo"
USER_GROUP="ludelafo"
USER_UID=1000
USER_GID=1000
USER_GROUPS=" \
	docker \
	proc \
	users \
	vboxusers \
	wheel \
"
USER_SHELL="/usr/bin/zsh"

ROOT_PASSWORD=$(openssl rand -base64 20) # Random 20 characters password
DISABLE_ROOT_ACCOUNT=true

# Aliases
<< COMMENT
read -r -d '' ALIASES <<- EOL
    alias diff='diff --color=auto'
    alias grep='grep --color=auto'
    alias ls='ls --color=auto'
EOL
COMMENT

# Network
WIRELESS_INTERFACE_PRESENT=false
WIRELESS_INTERFACE_REGEX="wl[0-9a-z]*"
WIRELESS_INTERFACE_NEW_NAME="wireless0"

ETHERNET_INTERFACE_PRESENT=false
ETHERNET_INTERFACE_REGEX="en[0-9a-z]*"
ETHERNET_INTERFACE_NEW_NAME="ethernet0"

# Kernel parameters, modules and hooks
KERNEL_PARAMETERS=" \
	i915.fastboot=1 \
	i915.enable_fbc=1 \
	i915.enable_gvt=1 \
	i915.enable_rc6=1 \
	i915.lvds_downclock=1 \
	acpi_backlight=video \
	kernel.nmi_watchdog=0 \
	libata.allow_tpm=1 \
	quiet \
	rw \
	vm.dirty_writeback_centisecs=6000 \
	vm.laptop_mode = 5 \
"
MODULES="i915 kvmgt vfio-iommu-type1 vfio-mdev"
HOOKS="base udev keyboard autodetect keymap consolefont modconf block filesystems fsck"

USE_STARTUP_NSH_SCRIPT=false

# Time zone
TIME_ZONE_REGION="Europe"
TIME_ZONE_CITY="Zurich"

# Locales
LOCALES=" \
	de_CH.UTF-8 \
	en_US.UTF-8 \
	fr_CH.UTF-8 \
	it_CH.UTF-8 \
"

DEFINED_LANG="en_US.UTF-8"
DEFINED_LC_COLLATE="C"
DEFINED_LC_TIME="fr_CH.UTF-8"

KEYMAP="" # "fr_CH-latin1"
FONT="lat2-16"
FONT_MAP="8859-2"

SWAPPINESS=10

# Packages
# linux or linux-lts
LINUX_KERNEL="linux-hardened"

CORE_PACKAGES="
	${LINUX_KERNEL} \
	${LINUX_KERNEL}-headers
	base \
	base-devel \
	linux-firmware \
	reflector \
"

UTILITIES_PACKAGES=" \
	alacritty \
	chezmoi \
	dkms \
	e2fsprogs \
	efibootmgr \
	exfat-utils \
	fd \
	fish \
	fuseiso \
	git \
	gparted \
	gsmartcontrol \
	gvfs-mtp \
	htop \
	lsd \
	man-db \
	mtpfs \
	ncdu \
	p7zip \
	powertop \
	rsync \
	tree \
	unrar \
	unzip \
	vim \
	xdg-user-dirs \
	xorg-host \
	zip \
"

NETWORK_PACKAGES=" \
	cifs-utils \
	cups \
	ethtool \
	firewalld \
	gvfs-nfs \
	iw \
	nfs-utils \
	reflector \
	sshfs \
	sshguard \
	sshfs \
	syncthing \
	system-config-printer \
	systemd-resolvconf \
	wpa_supplicant \
	wget \
"

SECURITY_PACKAGES=" \
	polkit \
	usbguard \
	veracrypt \
"

GUI_PACKAGES=" \
	clutter \
	ffmpegthumbnailer \
	glfw-wayland \
	glew-wayland \
	grim \
	gtk3 \
	gvfs \
	gvfs-smb \
	libinput \
	mako \
	mesa \
	libgsf \
	physlock \
	qt5-wayland \
	raw-thumbnailer \
	slurp \
	sway \
	swaybg \
	swaylock \
	swayidle \
	thunar \
	vulkan-intel \
	waybar \
	wayland \
	xorg-server-xwayland \
"

SOUND_AND_VIDEO_PACKAGES=" \
	alsa-utils \
	audacity \
	flac \
	gst-libav \
	gst-plugins-bad \
	gst-plugins-base \
	gst-plugins-good \
	gst-plugins-ugly \
	gstreamer \
	gstreamer-vaapi \
	lame \
	libmpeg2 \
	libtheora \
	libva-intel-driver \
	libva-utils \
	libvorbis \
	libvpx \
	mediainfo \
	mpv \
	opus \
	pulseaudio \
	pulseaudio-alsa \
	pulseaudio-bluetooth \
	vdpauinfo \
	vlc \
	wavpack \
	x264 \
	x265 \
"

DEVELOPER_TOOLS_PACKAGES=" \
	code \
	docker \
	docker-compose \
	jdk-openjdk \
	jre-openjdk \
	nodejs \
	npm \
	sqlitebrowser \
	virtualbox \
"

OFFICE_PACKAGES=" \
	atril \
	biber \
	libreoffice \
	pdfarranger \
	texlive-lang \
	texlive-most \
	texstudio \
"

INTERNET_PACKAGES=" \
	chromium \
	deluge \
	filezilla \
	firefox \
	flashplugin \
	icedtea-web \
	mktorrent \
	mkvtoolnix-cli \
	mkvtoolnix-gui \
	pepper-flash \
	telegram-desktop \
"

GRAPHICS_PACKAGES=" \
	displaycal \
	imv \
	gimp \
	inkscape \
"

GAMES_PACKAGES=" \
	wine \
	libretro-beetle-psx \
	libretro-bsnes \
	libretro-citra \
	libretro-desmume \
	libretro-gambatte \
	libretro-melonds \
	libretro-mgba
	libretro-nestopia \
	libretro-ppsspp \
	retroarch \
	retroarch-assets-xmb \
"

FONTS_PACKAGES=" \
	adobe-source-code-pro-fonts \
	adobe-source-han-sans-otc-fonts \
	adobe-source-han-serif-otc-fonts \
	bdf-unifont \
	dina-font \
	font-bh-ttf \
	font-mathematica \
	gentium-plus-font \
	gnu-free-fonts \
	noto-fonts-emoji \
	otf-latin-modern \
	otf-latinmodern-math \
	powerline-fonts \
	tamsyn-font \
	terminus-font \
	tex-gyre-fonts \
	ttf-anonymous-pro \
	ttf-bitstream-vera \
	ttf-caladea \
	ttf-carlito \
	ttf-cascadia-code \
	ttf-croscore \
	ttf-dejavu \
	ttf-droid \
	ttf-fantasque-sans-mono \
	ttf-fira-mono \
	ttf-fira-code \
	ttf-hack \
	ttf-inconsolata \
	ttf-joypixels \
	ttf-linux-libertine \
	ttf-roboto \
	ttf-opensans \
	noto-fonts \
"

THEMES_AND_TWEAKS_PACKAGES=" \
	breeze \
	breeze-gtk \
	hicolor-icon-theme \
"

OTHER_PACKAGES=" \
	android-file-transfer \
"

AUR_PACKAGES=" \
	adwaita-qt \
	apache-tools \
	brave-bin \
	clipman \
	duplicati-latest \
	insomnia-designer \
	ly \
	ncpamixer \
	npapi-vlc \
	parsec-bin \
	redshift-wlr-gamma-control-git \
	sedutil \
	starship-bin \
	virtualbox-ext-oracle \
	virtualbox-guest-dkms \
	virtualbox-guest-iso \
	wofi-hg \
	zsa-wally \
"

PACKAGES=" \
	${UTILITIES_PACKAGES} \
	${NETWORK_PACKAGES} \
	${SECURITY_PACKAGES} \
	${GUI_PACKAGES} \
	${SOUND_AND_VIDEO_PACKAGES} \
	${DEVELOPER_TOOLS_PACKAGES} \
	${OFFICE_PACKAGES} \
	${INTERNET_PACKAGES} \
	${GRAPHICS_PACKAGES} \
	${GAMES_PACKAGES} \
	${FONTS_PACKAGES} \
	${THEMES_AND_TWEAKS_PACKAGES} \
	${OTHER_PACKAGES} \
"
