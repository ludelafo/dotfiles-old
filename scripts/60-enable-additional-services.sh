#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Enable TRIM
systemctl enable fstrim.timer

# Disable tty2
# systemctl disable getty@tty2.service

# Enable Ly deskop manager (on tty2)
# systemctl enable ly.service

# Enable USB security
systemctl enable usbguard.service

# Enable docker
systemctl enable docker.service
