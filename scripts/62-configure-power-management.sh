#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Audio power saving
cat > "/etc/modprobe.d/audio-powersave.conf" <<- EOL
	options snd_hda_intel power_save=1 power_save_controller=1
EOL

# Enable Intel Wireless cards power saving settings
cat > "/etc/modprobe.d/iwlwifi.conf" <<- EOL
	options iwlwifi power_scheme=3
	options iwlwifi power_save=1
	options iwlwifi uapsd_disable=0
EOL

# Enable Thinkpad Fan Control
cat > "/etc/modprobe.d/thinkpad-fan-control.conf" <<- EOL
	options thinkpad_acpi fan_control=1
EOL

# Enable USB autosuspend
cat > "/etc/modprobe.d/usb-autosuspend.conf" <<- EOL
	options usbcore autosuspend=5
EOL

cat > "/etc/udev/rules.d/50-usb-powersave.rules" <<- EOL
	ACTION=="add", SUBSYSTEM=="usb", TEST=="power/control", ATTR{power/control}="auto"
EOL

# Wired interface
cat > "/etc/udev/rules.d/50-wired-powersave.rules" <<- EOL
	# Disable wired interface
	ACTION=="add", SUBSYSTEM=="net", NAME=="${ETHERNET_INTERFACE_NEW_NAME}", RUN+="/usr/bin/ip link set dev \$name down"

	# Disable wired interface Wake On LAN
	ACTION=="add", SUBSYSTEM=="net", NAME=="${ETHERNET_INTERFACE_NEW_NAME}", RUN+="/usr/bin/ethtool -s \$name wol g"
EOL

# Wireless interface
cat > "/etc/udev/rules.d/50-wireless-powersave.rules" <<- EOL
	# Enable powersaving on wireless interface
	ACTION=="add", SUBSYSTEM=="net", NAME=="${WIRELESS_INTERFACE_NEW_NAME}", RUN+="/usr/bin/iw dev \$name set power_save on"
EOL

# PCI Runtime power management
cat > "/etc/udev/rules.d/50-pci-powersave.rules" <<- EOL
	ACTION=="add", SUBSYSTEM=="pci", ATTR{power/control}="auto"
EOL

# Suspend when battery is at specified percentage
cat > "/etc/udev/rules.d/50-suspend-powersave.rules" <<- EOL
	SUBSYSTEM=="power_supply", ATTR{status}=="Discharging", ATTR{capacity}=="5", RUN+="/usr/bin/systemctl suspend"
EOL

# systemd tmpfiles
cat > "/etc/tmpfiles.d/pcie-aspm-powersave.conf" <<- EOL
	w /sys/module/pcie_aspm/parameters/policy - - - - powersave
EOL
