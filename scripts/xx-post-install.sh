#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Exit immediately if a command exits with a non-zero status
set -o errexit

# Treat unset variables as an error when substituting
set -o nounset

# The return value of a pipeline is the status of
# the last command to exit with a non-zero status,
# or zero if no command exited with a non-zero status
set -o pipefail

# Functions
renameInterface() {
	interfaceRegex="$1"
	interfaceNewName="$2"

	interfaceName=$(ip link show | grep --no-filename --only-matching "${interfaceRegex}:" | rev | cut -c 2- | rev)

	if [[ ${interfaceName} ]]; then
		interfaceMacAddress=$(cat "/sys/class/net/${interfaceName}/address")

		echo "SUBSYSTEM==\"net\", ACTION==\"add\", ATTR{address}==\"${interfaceMacAddress}\", NAME=\"${interfaceNewName}\"" >> "/etc/udev/rules.d/10-network.rules"

		# Reload the network configuration
		udevadm --debug test "/sys/class/net/${interfaceName}"
	fi
}

enableDhcpOnInterface() {
	interface="$1"

	systemctl start dhcpcd@${interface}.service
}

enableFallbackProfileOnInterface() {
	interface="$1"
	fallbackIpAddress="$2"
	fallbackIpRouters="$3"
	fallbackDns="$4"

	cat >> "/etc/dhcpcd.conf" <<- EOL
		profile static_${interface}
		static ip_address=${fallbackIpAddress}
		static routers=${fallbackIpRouters}
		static domain_name_servers=${fallbackDns}

		interface ${interface}
		fallback static_${interface}
	EOL
}

# Load the constants
source "./consts.sh"

EFI_BLOCK_DEVICE="${BASE_BLOCK_DEVICE}${EFI_PARTITION_NUMBER}"
ROOT_BLOCK_DEVICE="${BASE_BLOCK_DEVICE}${ROOT_PARTITION_NUMBER}"
SWAP_BLOCK_DEVICE="${BASE_BLOCK_DEVICE}${SWAP_PARTITION_NUMBER}"

# Localization
for locale in ${LOCALES}; do
	sed --in-place "s/^#${locale}/${locale}/" "/etc/locale.gen"
done

locale-gen

cat > "/etc/locale.conf" <<- EOL
	LANG=${DEFINED_LANG}
	LC_TIME=${DEFINED_LC_TIME}
	LC_COLLATE=${DEFINED_LC_COLLATE}
EOL

# Set the console keyboard layout
if [[ -n "${KEYMAP}" ]]; then
	echo "KEYMAP=${KEYMAP}" >> "/etc/vconsole.conf"
fi

echo "FONT=${FONT}" >> "/etc/vconsole.conf"
echo "FONT_MAP=${FONT_MAP}" >> "/etc/vconsole.conf"

# Configure pacman
sed --in-place "/\[multilib\]/,/Include/""s/^#//" "/etc/pacman.conf"
sed --in-place "s/#Color/Color/" "/etc/pacman.conf"
sed --in-place "s/#TotalDownload/TotalDownload/" "/etc/pacman.conf"

# Synchronize the mirrors
pacman --sync --refresh

# Sort the mirrors list
reflector --country Switzerland --country Italy --country France --country Germany --age 24 --protocol https --sort rate --save /etc/pacman.d/mirrorlist





# Users and user groups
groupadd --gid ${USER_GID} "${USER_GROUP}"

useradd --uid ${USER_UID} --gid ${USER_GID} --create-home --shell "${USER_SHELL}" "${USER_USERNAME}"

echo "%wheel ALL=(ALL) ALL" > "/etc/sudoers.d/wheel"

chmod -c 0440 "/etc/sudoers.d/wheel"

usermod --groups wheel "${USER_USERNAME}"

# Set the password for the user
passwd "${USER_USERNAME}"

# Install AUR helper
git clone "https://aur.archlinux.org/yay.git" "/tmp/yay"
chown ${USER_USERNAME}:${USER_USERNAME} "/tmp/yay"
cd "/tmp/yay"
sudo --user ${USER_USERNAME} makepkg --syncdeps --install --noconfirm

# Install AUR packages
sudo --user ${USER_USERNAME} yay --sync --needed ${AUR_PACKAGES}

# Enable TRIM
systemctl enable fstrim.timer

# Set the time zone
ln --symbolic --force "/usr/share/zoneinfo/${TIME_ZONE_REGION}/${TIME_ZONE_CITY}" "/etc/localtime"

# Set the hardware clock
hwclock --systohc

# Enable the NTP daemon
systemctl enable systemd-timesyncd.service

# Set the hostname
echo "${HOSTNAME}" > "/etc/hostname"

# Set the hosts
cat > "/etc/hosts" <<- EOL
	# Static table lookup for hostnames.
	# See hosts(5) for details.

	127.0.0.1	localhost
	::1		localhost
	127.0.1.1	${HOSTNAME}.localdomain	${HOSTNAME}
	::1		${HOSTNAME}.localdomain	${HOSTNAME}
EOL

# Network interfaces configuration
if [[ "${WIRELESS_INTERFACE_PRESENT}" = true ]]; then
	renameInterface "${WIRELESS_INTERFACE_REGEX}" "${WIRELESS_INTERFACE_NEW_NAME}"

	if [[ "${ENABLE_DHCP_ON_WIRELESS_INTERFACE}" = true ]]; then
		enableDhcpOnInterface "${WIRELESS_INTERFACE_NEW_NAME}"
	fi

	if [[ "${ENABLE_FALLBACK_PROFILE_ON_WIRELESS_INTERFACE}" = true ]]; then
		enableFallbackProfileOnInterface "${WIRELESS_INTERFACE_NEW_NAME}" "${WIRELESS_INTERFACE_FALLBACK_IP_ADDRESS}" "${WIRELESS_INTERFACE_FALLBACK_ROUTERS}" "${WIRELESS_INTERFACE_FALLBACK_DNS}"
	fi

	if [[ "${ENABLE_WPA_SUPPLICANT_HOOK}" = true ]]; then
		ln -s "/usr/share/dhcpcd/hooks/10-wpa_supplicant" "/usr/lib/dhcpcd/dhcpcd-hooks/"
	fi
fi

if [[ "${ETHERNET_INTERFACE_PRESENT}" = true ]]; then
	renameInterface "${ETHERNET_INTERFACE_REGEX}" "${ETHERNET_INTERFACE_NEW_NAME}"

	if [[ "${ENABLE_DHCP_ON_ETHERNET_INTERFACE}" = true ]]; then
		enableDhcpOnInterface "${ETHERNET_INTERFACE_NEW_NAME}"
	fi

	if [[ "${ENABLE_FALLBACK_PROFILE_ON_ETHERNET_INTERFACE}" = true ]]; then
		enableFallbackProfileOnInterface "${ETHERNET_INTERFACE_NEW_NAME}" "${ETHERNET_INTERFACE_FALLBACK_IP_ADDRESS}" "${ETHERNET_INTERFACE_FALLBACK_ROUTERS}" "${ETHERNET_INTERFACE_FALLBACK_DNS}"
	fi
fi

if [[ "${DISABLE_ARP_PROBING}" = true ]]; then
	echo "noarp" >> "/etc/dhcpcd.conf"
fi

# Security
usbguard add-user "${USER_USERNAME}"

systemctl enable firewalld.service
systemctl disable getty@tty2.service
systemctl enable ly.service
systemctl enable sshguard.service
systemctl enable usbguard.service




# Install the boot loader
efibootmgr --disk ${BASE_BLOCK_DEVICE} --part ${EFI_PARTITION_NUMBER} --create --label ${EFI_BOOT_ENTRY_LABEL} --loader "/vmlinuz-${LINUX_KERNEL}" --unicode "root=${ROOT_BLOCK_DEVICE} initrd=\initramfs-${LINUX_KERNEL}.img ${KERNEL_PARAMETERS}"

if [[ "$USE_STARTUP_NSH_SCRIPT" = true ]]; then
	echo "vmlinuz-${LINUX_KERNEL} root=${ROOT_BLOCK_DEVICE} initrd=\initramfs-${LINUX_KERNEL}.img ${KERNEL_PARAMETERS}" > "/boot/startup.nsh"
fi

# Set the root password
(
	echo "${ROOT_PASSWORD}"
	echo "${ROOT_PASSWORD}"
) | passwd root

if [[ "${DISABLE_ROOT_ACCOUNT}" = true ]]; then
	passwd --lock root
fi

# Add user to groups
usermod --groups $(echo ${USER_GROUPS} | tr ' ' ,) "${USER_USERNAME}"

# Done
read -p "Install done. Press any key to exit the arch-chroot environment."
