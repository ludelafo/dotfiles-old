#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Partition the disk
(
    echo "g" # Create a new empty GPT partition table

    # First partition (EFI)
    echo "n" # Add a new partition
    echo   # Partition number (accept default)
    echo   # First sector (accept default)
    echo "+${EFI_PARTITION_SIZE}" # Last sector

    # Second parition (root)
    echo "n" # Add a new partition
    echo   # Partition number (accept default)
    echo   # First sector (Accept default)
    echo "-${SWAP_PARTITION_SIZE}" # Last sector (all the available space minus the swap space)

    # Last partition (swap)
    echo "n" # Add a new partition
    echo   # Partition number (accept default)
    echo   # First sector (Accept default)
    echo   # Last sector (Accept default)

    # Change partition type for the first partition
    echo "t" # Change the partition type
    echo "1" # Partition number
    echo "1" # Partition type: EFÎ System

    # Change partition type for the second partition
    echo "t" # Change the partition type
    echo "2" # Partition number
    echo "20" # Partition type: Linux filesystem

    # Change partition type for the last partition
    echo "t" # Change the partition type
    echo "3" # Partition number
    echo "19" # Partition type: Linux swap

    echo "w" # Write changes
) | fdisk "${BASE_BLOCK_DEVICE}"