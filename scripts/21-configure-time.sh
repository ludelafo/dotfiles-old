#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Set the time zone
ln --symbolic --force "/usr/share/zoneinfo/${TIME_ZONE_REGION}/${TIME_ZONE_CITY}" "/etc/localtime"

# Set the hardware clock
hwclock --systohc

# Enable the NTP daemon
systemctl enable systemd-timesyncd.service
