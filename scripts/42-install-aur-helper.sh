#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Install required packages for the AUR helper
pacman --sync --needed --noconfirm git

# Clone the AUR helper using git
sudo --user "${USER_USERNAME}" git clone "https://aur.archlinux.org/yay.git" "/tmp/yay"

# Switch to the AUR directory
cd "/tmp/yay"

# Install the AUR helper
sudo --user "${USER_USERNAME}" makepkg --syncdeps --install --noconfirm

# Remove the cloned directory
rm --recursive "/tmp/yay"
