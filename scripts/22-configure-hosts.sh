#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Set the hostname
echo "${HOSTNAME}" > "/etc/hostname"

# Set the hosts
cat > "/etc/hosts" <<- EOL
	# Static table lookup for hostnames.
	# See hosts(5) for details.

	127.0.0.1	localhost
	::1		localhost
	127.0.1.1	${HOSTNAME}.localdomain	${HOSTNAME}
	::1		${HOSTNAME}.localdomain	${HOSTNAME}
EOL