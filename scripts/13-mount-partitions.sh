#!/usr/bin/env sh
#################################################
# Authors
#   Ludovic Delafontaine <ludovic.delafontaine@gmail.com>
# Documentation
#   https://gitlab.com/ludelafo/dotfiles
#################################################

# Get the current directory
full_path=$(realpath "${0}")
directory_path=$(dirname "${full_path}")

# Load the configuration
source "${directory_path}/00-config.sh"

# Mount the file systems
mount "${ROOT_BLOCK_DEVICE}" "${MOUNT_POINT}"
mkdir "${MOUNT_POINT}/boot"
mount "${EFI_BLOCK_DEVICE}" "${MOUNT_POINT}/boot"